# -*- coding: utf-8 -*-
# MIT License
# 
# Copyright (c) 2016 Amar Prakash Pandey
# Copyright (c) 2018 Aidan Kahrs
# Copyright (c) 2018 Regina Locicero
# Copyright (c) 2018 Calvin Wu
# Copyright (c) 2018 Quintin Reed
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# import library here
import pygame
import time
import random
from os import path
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from sugar3.graphics.style import GRID_CELL_SIZE


class gameClass:
    def __init__(self):
        # Set up a clock for managing the frame rate.
        self.clock = pygame.time.Clock()
        # contant value initialised
        self.white = (255,255,255)
        self.black = (0,0,0)
        self.red = (255,0,0)
        self.green = (0,155,0)
        self.yellow = (255, 255, 0)

        self.screen = None

        # path for the image folder
        assets = path.join(path.dirname(__file__), 'assets')

        # image loading
        self.snakeimg = pygame.image.load(path.join(assets + '/snake-head.png'))
        self.snakebody = pygame.image.load(path.join(assets + '/snake-body.png'))
        self.snaketail = pygame.image.load(path.join(assets + '/snake-tail.png'))
        self.appleimg = pygame.image.load(path.join(assets + '/apple.png'))
        self.startScreen = pygame.image.load(path.join(assets + '/start-screen.png'))
        self.endScreen = pygame.image.load(path.join(assets + '/end-screen.png'))

        # moving block size
        self.block = 20
        self.appleSize = 30

        # snake image direction variable
        self.direction = "right"

        # init font object with font size 25
        pygame.font.init()
        self.smallfont = pygame.font.Font(None, 20)
        self.medfont = pygame.font.Font(None, 40)
        self.largefont = pygame.font.Font(None, 70)

        # game state variables
        self.gameStarted = False
        self.gameOver = False
        self.paused = False

        # score
        self.gameScore = 0

    def set_paused(self, paused):
        self.paused = paused

    # function to print score
    def printScore(self,score):
        text = self.medfont.render("Score : " + str(score), True, self.black)
        self.screen.blit(text, [2,2])

    # function for generating 4 unique random number tuples for multiplication
    def getNumPairs(self):
        return random.sample([[x, y] for x in range(2, 13, 1) for y in range(2, 13, 1)], 4)  # get numbers from 2 to 12

    # function for generating points for the apples
    # generates 4 unique points that are at least an appleSize distance away from each other
    # also makes make sure that the apple is generated at least 10 appleSizes away from the snake's head
    def getAppleCoords(self, snake_x, snake_y):
        radius = self.appleSize
        largeRadius = radius * 10
        rangeX = (0, pygame.display.Info().current_h - self.appleSize)
        rangeY = (0, pygame.display.Info().current_h - self.appleSize)
        qty = 4  # or however many points you want

        deltas = set()
        # Generate a set of all points within appleSize of the origin, to be used as offsets later
        for x in range(-radius, radius + 1):
            for y in range(-radius, radius + 1):
                if x * x + y * y <= radius * radius:
                    deltas.add((x, y))

        randPoints = []
        excluded = set()
        # Generate a set of all points within 10 appleSizes of the snake head, to be used as offsets later
        for x in range(snake_x - largeRadius, (snake_x + 1) + largeRadius):
            for y in range(snake_y - largeRadius, (snake_y + 1) + largeRadius):
                if x * x + y * y <= largeRadius * largeRadius:
                    excluded.add((x, y))

        i = 0
        while i < qty:
            x = random.randrange(*rangeX)
            y = random.randrange(*rangeY)
            if (x, y) in excluded: continue
            randPoints.append((x, y))
            i += 1
            excluded.update((x + dx, y + dy) for (dx, dy) in deltas)
        return randPoints

    # function to print multiplication problem
    def problem(self, numberOne, numberTwo):
        text = self.medfont.render(str(numberOne) + " X " + str(numberTwo) + " = ?", True, self.black)
        self.screen.blit(text, [pygame.display.Info().current_h / 2, 2])

    # function for putting the number label on the apple
    def putNumInApple(self, num, (x, y)):
        label = self.medfont.render(str(num), True, self.yellow)
        self.screen.blit(label, [x, y])

    # score getter
    def get_score(self):
        return self.gameScore

    # Called to save the state of the game to the Journal.
    def write_file(self, file_path):
        pass

    # Called to load the state of the game from the Journal.
    def read_file(self, file_path):
        pass

    # to generate and update snake :P
    def snake(self, block, snakeList):
        # At some point, we may want to rotate the snake's body when it reaches
        # a part where the snake turns
        snakebody = pygame.transform.rotate(self.snakebody, 0)
        tail = pygame.transform.rotate(self.snaketail, 0)

        if direction == "right":
            head = pygame.transform.rotate(self.snakeimg, 270)
            snakebody = pygame.transform.rotate(self.snakebody, 270)

        if direction == "left":
            head = pygame.transform.rotate(self.snakeimg, 90)
            snakebody = pygame.transform.rotate(self.snakebody, 90)

        if direction == "up":
            head = self.snakeimg
            snakebody = self.snakebody

        if direction == "down":
            head = pygame.transform.rotate(self.snakeimg, 180)
            snakebody = pygame.transform.rotate(self.snakebody, 180)


        self.screen.blit(head, (snakeList[-1][0], snakeList[-1][1]))
        for XnY in snakeList[:-1]:
            self.screen.blit(snakebody,(XnY[0], XnY[1]))


    def text_object(self, msg, color,size):
        if size == "small":
            textSurface = self.smallfont.render(msg, True, color)
            return textSurface, textSurface.get_rect()

        if size == "medium":
            textSurface = self.medfont.render(msg, True, color)
            return textSurface, textSurface.get_rect()

        if size == "large":
            textSurface = self.largefont.render(msg, True, color)
            return textSurface, textSurface.get_rect()

    # func to print message on game display
    def message_to_display(self, msg, color, y_displace = 0, size = "small"):
        textSurf , textRect = self.text_object(msg, color, size)
        textRect.center = (pygame.display.Info().current_h/2), (pygame.display.Info().current_h/2) + y_displace
        self.screen.blit(textSurf, textRect)

    def showStartScreen(self):
        while Gtk.events_pending():
                Gtk.main_iteration()
        self.screen.blit(self.startScreen,(0,0))
        myfont=self.largefont
        nlabel=myfont.render("Sugar Snake", 1, self.white)
        dlabel=self.medfont.render("Press the play button in the top left corner to begin.", 1, self.white)
        self.screen.blit(nlabel, (200,200))
        self.screen.blit(dlabel, (400,400))
        pygame.display.update()
    
    def showGameoverScreen(self):
        while Gtk.events_pending():
                Gtk.main_iteration()
        self.screen.blit(self.endScreen,(0,0))
        self.message_to_display("Game Over", self.red, -70, "large")
        text = self.medfont.render("Your final score is : " + str(self.get_score()), True, self.red)
        self.screen.blit(text, [100,100])
        pygame.display.update()
        
    # game starts here
    def run(self):
        # global variable direction
        global direction
        self.screen = pygame.display.get_surface()

        direction = "right"

        # variable init
        running = True

        # snake variables
        snakeList = []
        snakeLength = 1

        # array of apple coordinates tuples
        appleCoords = self.getAppleCoords(0, 0)

        # apple coordinates tuples
        rightAppleCoords = appleCoords[0]
        wrongApple1Coords = appleCoords[1]
        wrongApple2Coords = appleCoords[2]
        wrongApple3Coords = appleCoords[3]

        # apple x and y coordinates
        rightAppleX = rightAppleCoords[0]
        rightAppleY = rightAppleCoords[1]
        wrongApple1X = wrongApple1Coords[0]
        wrongApple1Y = wrongApple1Coords[1]
        wrongApple2X = wrongApple2Coords[0]
        wrongApple2Y = wrongApple2Coords[1]
        wrongApple3X = wrongApple3Coords[0]
        wrongApple3Y = wrongApple3Coords[1]

        # array of number pair tuples
        numPairs = self.getNumPairs()

        # number pair tuples
        correctNumPair = numPairs[0]
        wrongNumPair1 = numPairs[1]
        wrongNumPair2 = numPairs[2]
        wrongNumPair3 = numPairs[3]

        start_x = pygame.display.Info().current_h/2
        start_y = pygame.display.Info().current_h/2

        move_to_h = 10
        move_to_v = 0

        while running :
            # Pump GTK messages.
            while Gtk.events_pending():
                Gtk.main_iteration()
            if self.gameStarted == False:
                self.showStartScreen()
                continue
            elif self.gameOver == True:
                self.showGameoverScreen()
            else:
                # Pump PyGame messages
                if self.paused:
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_SPACE:
                               self.set_paused(not self.paused)
                else:
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_LEFT and move_to_h == 0:
                                direction = "left"
                                move_to_h = -self.block
                                move_to_v = 0
                            elif event.key == pygame.K_RIGHT and move_to_h == 0:
                                direction = "right"
                                move_to_h = self.block
                                move_to_v = 0
                            elif event.key == pygame.K_UP and move_to_v == 0:
                                direction = "up"
                                move_to_v = -self.block
                                move_to_h = 0
                            elif event.key == pygame.K_DOWN and move_to_v == 0:
                                direction = "down"
                                move_to_v = self.block
                                move_to_h = 0
                            elif event.key == pygame.K_SPACE:
                                self.set_paused(True)

                    if start_x >= pygame.display.Info().current_h or start_x < 0 or start_y >= pygame.display.Info().\
                            current_h or start_y < 0:
                        self.gameOver = True

                    start_x += move_to_h
                    start_y += move_to_v

                    self.screen.fill(self.white)
                    self.screen.blit(self.appleimg, rightAppleCoords)
                    self.screen.blit(self.appleimg, wrongApple1Coords)
                    self.screen.blit(self.appleimg, wrongApple2Coords)
                    self.screen.blit(self.appleimg, wrongApple3Coords)

                    snakeHead = []
                    snakeHead.append(start_x)
                    snakeHead.append(start_y)
                    snakeList.append(snakeHead)

                    if len(snakeList) > snakeLength:
                        del snakeList[0]

                    self.printScore(snakeLength - 1)

                    self.problem(correctNumPair[0], correctNumPair[1])

                    self.putNumInApple((correctNumPair[0] * correctNumPair[1]), rightAppleCoords)
                    self.putNumInApple((wrongNumPair1[0] * wrongNumPair1[1]), wrongApple1Coords)
                    self.putNumInApple((wrongNumPair2[0] * wrongNumPair2[1]), wrongApple2Coords)
                    self.putNumInApple((wrongNumPair3[0] * wrongNumPair3[1]), wrongApple3Coords)

                    self.snake(self.block, snakeList)
                    pygame.display.update()

                    # to see if snake has eaten himself or not
                    for eachSegment in snakeList[:-1]:
                        if eachSegment == snakeHead:
                            self.snake(self.block, snakeList)
                            pygame.time.delay(1000)
                            self.gameOver = True

                    if start_x > rightAppleX and start_x < rightAppleX + self.appleSize or start_x + self.block > \
                            rightAppleX and start_x + self.block < rightAppleX + self.appleSize:
                        if start_y > rightAppleY and start_y < rightAppleY + self.appleSize:
                            # Re-generate the apple coordinates
                            appleCoords = self.getAppleCoords(start_x, start_y)
                            rightAppleCoords = appleCoords[0]
                            wrongApple1Coords = appleCoords[1]
                            wrongApple2Coords = appleCoords[2]
                            wrongApple3Coords = appleCoords[3]
                            rightAppleX = rightAppleCoords[0]
                            rightAppleY = rightAppleCoords[1]
                            wrongApple1X = wrongApple1Coords[0]
                            wrongApple1Y = wrongApple1Coords[1]
                            wrongApple2X = wrongApple2Coords[0]
                            wrongApple2Y = wrongApple2Coords[1]
                            wrongApple3X = wrongApple3Coords[0]
                            wrongApple3Y = wrongApple3Coords[1]

                            # Regenerate the num pairs
                            numPairs = self.getNumPairs()
                            correctNumPair = numPairs[0]
                            wrongNumPair1 = numPairs[1]
                            wrongNumPair2 = numPairs[2]
                            wrongNumPair3 = numPairs[3]

                            snakeLength += 1

                        if start_y + self.block > rightAppleY and start_y + self.block < rightAppleY + self.appleSize:
                            # Re-generate the apple coordinates
                            appleCoords = self.getAppleCoords(start_x, start_y)
                            rightAppleCoords = appleCoords[0]
                            wrongApple1Coords = appleCoords[1]
                            wrongApple2Coords = appleCoords[2]
                            wrongApple3Coords = appleCoords[3]
                            rightAppleX = rightAppleCoords[0]
                            rightAppleY = rightAppleCoords[1]
                            wrongApple1X = wrongApple1Coords[0]
                            wrongApple1Y = wrongApple1Coords[1]
                            wrongApple2X = wrongApple2Coords[0]
                            wrongApple2Y = wrongApple2Coords[1]
                            wrongApple3X = wrongApple3Coords[0]
                            wrongApple3Y = wrongApple3Coords[1]

                            # Regenerate the num pairs
                            numPairs = self.getNumPairs()
                            correctNumPair = numPairs[0]
                            wrongNumPair1 = numPairs[1]
                            wrongNumPair2 = numPairs[2]
                            wrongNumPair3 = numPairs[3]

                            snakeLength += 1

                    if start_x > wrongApple1X and start_x < wrongApple1X + self.appleSize or start_x + self.block > \
                            wrongApple1X and start_x + self.block < wrongApple1X + self.appleSize:
                        if start_y > wrongApple1Y and start_y < wrongApple1Y + self.appleSize:
                            self.gameOver = True

                    if start_x > wrongApple2X and start_x < wrongApple2X + self.appleSize or start_x + self.block > \
                            wrongApple2X and start_x + self.block < wrongApple2X + self.appleSize:
                        if start_y > wrongApple2Y and start_y < wrongApple2Y + self.appleSize:
                            self.gameOver = True

                    if start_x > wrongApple3X and start_x < wrongApple3X + self.appleSize or start_x + self.block > \
                            wrongApple3X and start_x + self.block < wrongApple3X + self.appleSize:
                        if start_y > wrongApple3Y and start_y < wrongApple3Y + self.appleSize:
                            self.gameOver = True

                    self.gameScore = snakeLength-1
            # initialising no. of frames per sec
            self.clock.tick(10)


# # this fuction kicks-off everything 
def main():
    pygame.init()
    screen=pygame.display.set_mode((0, 0), pygame.RESIZABLE)
    game = gameClass()
    game.run()
    pygame.display.quit()
    pygame.quit()
    sys.exit(0)

if __name__ == '__main__':
    main()


