# hfoss-sugar-snake
This is our final project for [RIT's HFOSS class](https://github.com/ritjoe/hfoss) in spring 2018. It is built using python 3 with [pygame](https://github.com/pygame/pygame). Our game is meant to be run on the Sugar operating system as an educational activity reviewing the multiplication tables for 4th grade students. It is based off of a simple snake game called [Plaked](https://github.com/amarlearning/Plaked). 

# How to Play
The objective of the game is to eat the apples that have the correct solution to the displayed multiplication problem. The more apples with the correct answer you eat, the longer you get. The game runs until you hit into yourself, the window boundary, or an apple with the wrong answer. You can press the spacebar to pause the game.

# How It Works
Four unique random number pairs from 2 to 12 are generated.

One of them is chosen to be the problem.

The products of the number pairs are put onto apples.

The apple coordinates are unique, at least the size of an apple away from each other, and at least 10 apple sizes away from the snakes head.

The number pairs and apple coordinates are regenerated when the snake has eaten the apple with the correct answer.

# Licensing
The code is under MIT license which is included in our repo. The art assets in the asset folder are under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode).

# How to Install and Run the Game

## Running the Game in Production
**If you just want to play the game these are the instructions you want to follow**; this is also the instructions you should follow if you want to distribute your own version of the game. This process will generate the .xo file necessary.
You should have Sugar running already.

First you want to open a terminal instance, (press F3 if stuck in the journal). Search for terminal to find it.

Then clone our github repo by running this command:

    git clone https://github.com/axk4545/hfoss-sugar-snake

Then cd into the root directory:

    cd hfoss-sugar-snake

Following the [Sugar Quickstart](https://github.com/FOSSRIT/sugar-quickstart), you want to run the following commands to generate an .xo file.

    ./setup genpot
    ./setup build
    ./setup dist_xo

You should now see an hfoss-snake-1.xo file when you list the contents of the folder using:

    ls -la

Now to install the .xo file run:

    sugar-install-bundle hfoss-snake-1.xo

If you restart your sugar installation you should be able to search "hfoss-snake" and be able to run the game from there.

## For Development
Run Sugar on VirtualBox. Most of us use Fedora based sugar. Make sure to remember to do a liveinst from the terminal so you have persistence on your VM.

Press F3 to go to the main menu if stuck in journal page.

Click on the search bar and search for terminal.

In the terminal run 

    git clone https://github.com/axk4545/hfoss-sugar-snake

Then cd into the root directory:

    cd hfoss-sugar-snake

To run the game run

    sugar-activity snakeActivity.snakeActivity

Note: any changes you make will show up when re-running the previous command.

